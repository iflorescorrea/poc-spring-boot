package com.latam.spring.demo.controller;

import com.latam.spring.demo.controller.domain.Account;
import com.latam.spring.demo.service.AccountService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/account")
public class IndexController {

    private AccountService accountService;

    public IndexController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PutMapping
    public long add(@RequestBody Account account) {
        return accountService.add(account);
    }

    @GetMapping("/{id}")
    public Account get(@PathVariable long id) {
        return accountService.get(id);
    }

    @GetMapping()
    public List<Account> list() {
        return accountService.list();
    }
}
