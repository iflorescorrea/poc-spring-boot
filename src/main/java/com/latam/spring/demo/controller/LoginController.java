package com.latam.spring.demo.controller;

import com.latam.spring.demo.controller.domain.Account;
import com.latam.spring.demo.service.AccountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {

    private AccountService accountService;

    public LoginController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping()
    public Account login(@RequestHeader(name = "X-Auth")String login) {
        return accountService.login(login);
    }
}
