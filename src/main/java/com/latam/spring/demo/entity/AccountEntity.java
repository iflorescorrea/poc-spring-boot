package com.latam.spring.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountEntity extends AbstractPersistable<Long> {

    @Column(unique = true ,nullable = false, length = 50)
    private String name;

    @Column(name = "description", length = 50)
    private String description;

}
