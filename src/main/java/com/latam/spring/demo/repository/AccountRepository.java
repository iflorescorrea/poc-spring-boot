package com.latam.spring.demo.repository;


import com.latam.spring.demo.entity.AccountEntity;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<AccountEntity, Long> {

    AccountEntity findByName(String name);

}
