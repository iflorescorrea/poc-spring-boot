package com.latam.spring.demo.service;

import com.latam.spring.demo.controller.domain.Account;
import com.latam.spring.demo.entity.AccountEntity;
import com.latam.spring.demo.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {

    private AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public long add(Account account) {

        AccountEntity entiry = AccountEntity.builder().name(account.getName()).build();

        return accountRepository.save(entiry).getId();
    }

    public Account get(long id) {

        Optional<AccountEntity> opt = accountRepository.findById(id);

        return convert(opt.orElse(null));
    }

    public List<Account> list() {

        Iterable<AccountEntity> iterable = accountRepository.findAll();

        List<Account> list = new ArrayList<>();

        iterable.forEach(entity -> list.add(convert(entity)));

        return list;
    }

    private Account convert( AccountEntity entity) {
        if (entity != null) {
            return Account.builder().id(entity.getId()).name(entity.getName()).build();
        }
        return Account.builder().build();
    }

    public Account login(String name) {

        AccountEntity opt = accountRepository.findByName(name);

        return convert(opt);
    }

}
